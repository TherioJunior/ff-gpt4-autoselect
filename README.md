# Firefox GPT-4 Autoselect

This title technically doesn't make too much sense anymore, since this extension now includes a dropdown to let you choose your preferred model, but whatever.



### Download

You can download the Extension from the Firefox Add-on Store [here](https://addons.mozilla.org/en-US/firefox/addon/gpt-auto-model-selector/)

### Privacy?

This extension does not need your login or a API key or anything of the sort. It simply looks at and modifies the URL you're currently on if it contains "chat.openai.com".

### Why?

Because when you want to use GPT4 after starting a new chat/clearing your current one, you'd always find yourself having to manually select it. I find this annoying and so this extension was created

### What does it do?

It simply observes the URL and appends your model of choice to it, and then refreshes the website. A smoother way to do this is planned, though not yet implemented.

### Possible issues

None that I am aware of. Feel free to open a GitLab issue in case there is one.
