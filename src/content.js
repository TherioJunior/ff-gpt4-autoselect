const getDefaultGPTVersion = async () => {
    return new Promise((resolve) => {
        browser.storage.local.get('gpt_version', (data) => {
            const defaultVersion = data['gpt_version'] || 'gpt-4';

            resolve(defaultVersion);
        });
    });
}

const updateDefaultGPTVersion = async () => {
    const targetDomain = 'chat.openai.com';
    const currentURL = new URL(window.location.href);

    if (currentURL.pathname.startsWith('/c/') || currentURL.pathname.startsWith('/auth/'))
        return;

    // Check if current URL's domain matches the target domain
    if (currentURL.hostname === targetDomain) {
        const defaultVersion = await getDefaultGPTVersion();

        if (currentURL.searchParams.get('model') ===  defaultVersion) {
            return;
        }

        // Set or update the 'model' query param to the saved default model version
        currentURL.searchParams.set('model', defaultVersion);

        // Redirect to the chat page using the saved default model version
        window.location.href = currentURL.toString();
    }
}

function periodicallyCheckURL() {
    setInterval(() => {
        updateDefaultGPTVersion();
    }, 1000);
}

updateDefaultGPTVersion();
periodicallyCheckURL();
